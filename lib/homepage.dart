import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:indikatorlantai/utils.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  HomePage({Key? key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Progress> prog = Progress.getProg();
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  void _startTimer() {
    // Update the percentages every 5 seconds
    _timer = Timer.periodic(Duration(seconds: 5), (timer) {
      setState(() {
        // Update your progress data here
        prog = Progress.getProg();
      });
    });
  }

  @override
  void dispose() {
    // Dispose of the timer to prevent memory leaks
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularPercentIndicator(
              radius: prog[0].radius,
              lineWidth: prog[0].lineWidth,
              percent: prog[0].percent,
              animation: prog[0].animation,
              animationDuration: prog[0].animationDuration,
              progressColor: prog[0].progressColor,
              backgroundColor: prog[0].backgroundColor,
              circularStrokeCap: prog[0].circularStrokeCap,
              center: Center(
                child: Text(
                  "Lantai 1",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: prog[0].progressColor,
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),

            CircularPercentIndicator(
              radius: prog[1].radius,
              lineWidth: prog[1].lineWidth,
              percent: prog[1].percent,
              animation: prog[1].animation,
              animationDuration: prog[1].animationDuration,
              progressColor: prog[1].progressColor,
              backgroundColor: prog[1].backgroundColor,
              circularStrokeCap: prog[1].circularStrokeCap,
              center: Center(
                child: Text(
                  "Lantai 2",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: prog[1].progressColor,
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),

            CircularPercentIndicator(
              radius: prog[2].radius,
              lineWidth: prog[2].lineWidth,
              percent: prog[2].percent,
              animation: prog[2].animation,
              animationDuration: prog[2].animationDuration,
              progressColor: prog[2].progressColor,
              backgroundColor: prog[2].backgroundColor,
              circularStrokeCap: prog[2].circularStrokeCap,
              center: Center(
                child: Text(
                  "Lantai 3",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: prog[2].progressColor,
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),

            CircularPercentIndicator(
              radius: prog[3].radius,
              lineWidth: prog[3].lineWidth,
              percent: prog[3].percent,
              animation: prog[3].animation,
              animationDuration: prog[3].animationDuration,
              progressColor: prog[3].progressColor,
              backgroundColor: prog[3].backgroundColor,
              circularStrokeCap: prog[3].circularStrokeCap,
              center: Center(
                child: Text(
                  "Lantai 4",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: prog[3].progressColor,
                  ),
                ),
              ),
            ),

            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
